import { FormAddNumbersComponent } from './form-add-numbers.component';
import { NumbersDataService } from '../services/numbers-data.service';
import { NumbersData } from 'src/dto/numbers-data';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

describe('FormAddNumbersComponent', () => {
    let component: FormAddNumbersComponent;
    let service: NumbersDataService;
    let fakeNumData: NumbersData;

    beforeEach(() => {
        service = new NumbersDataService(null);
        component = new FormAddNumbersComponent(null, service);
        fakeNumData = new NumbersData();
        fakeNumData.numbers = '1,2,3,4,5';
        fakeNumData.creationDate = new Date();
        fakeNumData.updatedDate = new Date();
    });

    it('should save a number data by adding an ID to it by testing the service and the eventEmitter', () => {
        let tempFakeData: NumbersData;
        spyOn(service, 'saveNewNumberData').and.callFake((fakeNumData) => {
            return new Observable<NumbersData>().pipe(
                map((response: NumbersData) => {
                  fakeNumData.id = 1;
                  return fakeNumData;
                })
              )
        });

        component.addNumbersData();

        component.addedNewNumberData.subscribe(response => {
            tempFakeData = response;
            expect(tempFakeData.id).toBe(1);
        });
    });

    it('should update a number data by changing numbersData by testing the service and the eventEmitter', () => {
        let tempFakeData: NumbersData;
        component.editingNumberData = fakeNumData;
        component.numbersTextArea = '1,2,3,4,5,6';
        spyOn(service, 'updateNumberData').and.callFake((fakeNumData) => {
            return new Observable<NumbersData>().pipe(
                map((response: NumbersData) => {
                  return fakeNumData;
                })
              )
        });

        component.updateNumbersData();

        component.editedNumberData.subscribe(response => {
            tempFakeData = response;
            expect(tempFakeData.numbers).toBe('1,2,3,4,5,6');
        });
    });

})