import { NumbersDataService } from './../services/numbers-data.service';
import { ConfirmationQuestionComponent } from './../confirmation-question/confirmation-question.component';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NumbersData } from 'src/dto/numbers-data';

@Component({
  selector: 'app-form-add-numbers',
  templateUrl: './form-add-numbers.component.html',
  styleUrls: ['./form-add-numbers.component.scss']
})
export class FormAddNumbersComponent implements OnInit {

  @Output() addedNewNumberData = new EventEmitter<NumbersData>();
  @Output() editedNumberData = new EventEmitter<NumbersData>();

  numbersTextArea: string = '';
  isEditMode: boolean = false;
  editingNumberData: NumbersData = null;

  constructor(
    public dialog: MatDialog,
    private numbersDataService: NumbersDataService) { }

  ngOnInit() {
    
  }

  openClearTextareaDialog(): void {
    const dialogRef = this.dialog.open(ConfirmationQuestionComponent, {
      width: '350px',
      data: {
        isCancelUpdate: false
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.numbersTextArea = '';
      }
    });
  }

  openCancelUpdateDialog(): void {
    const dialogRef = this.dialog.open(ConfirmationQuestionComponent, {
      width: '350px',
      data: {
        isCancelUpdate: true
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.cancelEditingNumbersData();
      }
    });
  }

  public addNumbersData() {
    let numberData: NumbersData = new NumbersData();
    numberData.numbers = this.numbersTextArea;
    numberData.creationDate = new Date();
    numberData.updatedDate = new Date();
    this.numbersTextArea = '';
    this.numbersDataService.saveNewNumberData(numberData).subscribe(response => {
      this.addedNewNumberData.emit(response);
    });
  }

  public startEditingNumbersData(numbersData: NumbersData) {
    this.isEditMode = true;
    this.numbersTextArea = numbersData.numbers;
    this.editingNumberData = numbersData;
  }

  public cancelEditingNumbersData() {
    this.isEditMode = false;
    this.numbersTextArea = '';
    this.editingNumberData = null;
  }

  public updateNumbersData() {
    let numbersData: NumbersData = this.editingNumberData;
    numbersData.numbers = this.numbersTextArea;
    numbersData.updatedDate = new Date();
    this.numbersDataService.updateNumberData(numbersData).subscribe(response => {
      this.editedNumberData.emit(response);
      this.cancelEditingNumbersData();
    });
  }

}
