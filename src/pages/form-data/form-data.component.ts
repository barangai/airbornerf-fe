import { NumbersDataService } from './../services/numbers-data.service';
import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { NumbersData } from 'src/dto/numbers-data';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-form-data',
  templateUrl: './form-data.component.html',
  styleUrls: ['./form-data.component.scss']
})
export class FormDataComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @Output() editNumberData = new EventEmitter<NumbersData>();

  displayedColumns: string[] = ['id', 'numbers', 'creationDate', 'updatedDate', 'actions'];
  dataSource: MatTableDataSource<NumbersData>;
  numbersDataElements: NumbersData[] = [];

  constructor(private numbersService: NumbersDataService) {
    this.numbersService.getNumbersData().subscribe(response => {
      this.numbersDataElements = response;
      this.dataSource = new MatTableDataSource(this.numbersDataElements);
      this.dataSource.paginator = this.paginator;
    });

  }

  ngOnInit() {

  }

  public addNewElement(numbersData: NumbersData) {
    this.numbersDataElements.push(numbersData);
    this.dataSource = new MatTableDataSource(this.numbersDataElements);
  }

  public changeExistingElement(numbersData: NumbersData) {
    for(let i = 0; i < this.numbersDataElements.length; i++) {
      if(this.numbersDataElements[i].id == numbersData.id) {
        this.numbersDataElements[i].numbers = numbersData.numbers;
        this.numbersDataElements[i].updatedDate = numbersData.updatedDate;
      }
    }
    this.dataSource = new MatTableDataSource(this.numbersDataElements);
  }

  public clickedEdit(numbersData: NumbersData) {
    this.editNumberData.emit(numbersData);
  }

}
