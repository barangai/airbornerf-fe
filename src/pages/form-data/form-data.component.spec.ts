import { NumbersData } from './../../dto/numbers-data';
import { NumbersDataService } from './../services/numbers-data.service';
import { FormDataComponent } from './form-data.component';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

describe('FormDataComponent', () => {
    let component: FormDataComponent;
    let service: NumbersDataService;
    let fakeNumData: NumbersData;

    beforeEach(() => {
        service = new NumbersDataService(null);

        spyOn(service, 'getNumbersData').and.callFake(() => {
            return new Observable<NumbersData[]>().pipe(
                map((response: NumbersData[]) => {
                  fakeNumData.id = 1;
                  return [fakeNumData];
                })
              )
        });
        
        component = new FormDataComponent(service);
        fakeNumData = new NumbersData();
        fakeNumData.id = 1;
        fakeNumData.numbers = '1,2,3,4,5';
        fakeNumData.creationDate = new Date();
        fakeNumData.updatedDate = new Date();
    });

    it('should emit the eventEmitter', () => {
        component.clickedEdit(fakeNumData);

        component.editNumberData.subscribe(response => {
            fakeNumData = response;
            expect(fakeNumData.id).toBe(1);
            expect(fakeNumData.numbers).toBe('1,2,3,4,5');
        });
    });
})