import { environment } from './../../environments/environment.prod';
import { NumbersData } from './../../dto/numbers-data';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NumbersDataService {

  SERVER_URL: string = environment.server;
  API_CLASS_ENDPOINT: string = '/numbersData';
  API_GET_METHOD_ENDPOINT: string = '/getNumbersData';
  API_INSERT_METHOD_ENDPOINT: string = '/addNumbersData';
  API_UPDATE_METHOD_ENDPOINT: string = '/updateNumbersData';

  constructor(private httpClient: HttpClient) { }

  public getNumbersData() {
    return this.httpClient.get<NumbersData[]>(this.SERVER_URL + this.API_CLASS_ENDPOINT + this.API_GET_METHOD_ENDPOINT).pipe(
      map((result: NumbersData[]) => {
        return result;
      })
    )
  }

  public saveNewNumberData(numberData: NumbersData) {
    return this.httpClient.post<NumbersData>(this.SERVER_URL + this.API_CLASS_ENDPOINT + this.API_INSERT_METHOD_ENDPOINT, numberData).pipe(
      map((result: NumbersData) => {
        return result;
      })
    )
  }

  public updateNumberData(numberData: NumbersData) {
    return this.httpClient.put<NumbersData>(this.SERVER_URL + this.API_CLASS_ENDPOINT + this.API_UPDATE_METHOD_ENDPOINT, numberData).pipe(
      map((result: NumbersData) => {
        return result;
      })
    )
  }

}
