import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-confirmation-question',
  templateUrl: './confirmation-question.component.html',
  styleUrls: ['./confirmation-question.component.scss']
})
export class ConfirmationQuestionComponent implements OnInit {

  isUpdate: boolean;

  constructor(
    public dialogRef: MatDialogRef<ConfirmationQuestionComponent>,
    @Inject(MAT_DIALOG_DATA) data) { 
      this.isUpdate = data.isCancelUpdate;
    }

  ngOnInit() {
    
  }

  closeDialogWithNo() {
    this.dialogRef.close(false);
  }


  closeDialogWithYes() {
    this.dialogRef.close(true);
  }

}
