import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomepageComponent } from './homepage.component';
import { FormAddNumbersComponent } from './form-add-numbers/form-add-numbers.component';
import { FormDataComponent } from './form-data/form-data.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDialogModule } from '@angular/material/dialog';
import { ConfirmationQuestionComponent } from './confirmation-question/confirmation-question.component';

@NgModule({
  declarations: [HomepageComponent, FormAddNumbersComponent, FormDataComponent, ConfirmationQuestionComponent],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatDialogModule
  ],
  entryComponents: [ConfirmationQuestionComponent],
  bootstrap: [HomepageComponent]
})
export class HomepageModule { }