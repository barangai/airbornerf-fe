import { FormDataComponent } from './form-data/form-data.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormAddNumbersComponent } from './form-add-numbers/form-add-numbers.component';
import { NumbersData } from 'src/dto/numbers-data';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  @ViewChild(FormAddNumbersComponent) formAddNumbers: FormAddNumbersComponent;
  @ViewChild(FormDataComponent) formShowNumbers: FormDataComponent;

  constructor() { }

  ngOnInit() {
  }

  public addedNewNumberData(numbersData: NumbersData) {
    this.formShowNumbers.addNewElement(numbersData);
  }

  public editedNumberData(numbersData: NumbersData) {
    this.formShowNumbers.changeExistingElement(numbersData);
  }

  public editNumberData(numbersData: NumbersData) {
    this.formAddNumbers.startEditingNumbersData(numbersData);
  }

}
