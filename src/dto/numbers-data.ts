
export class NumbersData {
    id: number;
    numbers: string;
    creationDate: Date;
    updatedDate: Date;
}