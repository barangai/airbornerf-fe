function setUpFrontEnd() {
  cy.visit('http://localhost:4200');
}

describe('My First Test', function () {
  it('should have to correct title', () => {
    setUpFrontEnd();
    cy.title().should('eq', 'AirborneRFFe');
  });

  it('should get numbers data from server', () => {
    cy.request('GET', 'http://localhost:8080/numbersData/getNumbersData');
  });

  // it('should save a new number data ', () => {
  //   cy.request('POST', 'http://localhost:8080/numbersData/addNumbersData', 
  //   {
  //     id: null,
  //     numbers: '1,2,3,4,5',
  //     creationDate: new Date(),
  //     updatedDate: new Date()
  //   });
  // });

  it('should update the above created mock number data', () => {
    cy.request('PUT', 'http://localhost:8080/numbersData/updateNumbersData',
      {
        id: 1,
        numbers: '1,2,3,4,5,6',
        creationDate: new Date(),
        updatedDate: new Date()
      });
  });

})